var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "lib/silica"], function (require, exports, SilicaTS) {
    var State = SilicaTS.State;
    var Game = SilicaTS.Game;
    var Input = SilicaTS.Input;
    var Keys = SilicaTS.Keys;
    var Entity = SilicaTS.Entity;
    var Vector2 = SilicaTS.Vector2;
    var AppMain = (function () {
        function AppMain() {
        }
        AppMain.prototype.Run = function () {
            var s = new State();
            var game = new Game(document.body, s);
            game.Start();
            s.AddChild(new Player(new Vector2(100, 100), new Vector2(20, 20)));
        };
        return AppMain;
    })();
    exports.AppMain = AppMain;
    var Box = (function (_super) {
        __extends(Box, _super);
        function Box(position, size, color) {
            if (color === void 0) { color = "black"; }
            _super.call(this, position, size);
            this.color = color;
        }
        Box.prototype.Draw = function (ctx) {
            ctx.fillStyle = this.color;
            ctx.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);
        };
        return Box;
    })(Entity);
    exports.Box = Box;
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(position, size, color) {
            if (color === void 0) { color = "red"; }
            _super.call(this, position, size, color);
            this.color = color;
        }
        Player.prototype.Update = function () {
            if (Input.IsKeyDown(65 /* A */)) {
                this.position.x -= 1;
            }
            if (Input.IsKeyDown(68 /* D */)) {
                this.position.x += 1;
            }
            if (Input.IsKeyDown(87 /* W */)) {
                this.position.y -= 1;
            }
            if (Input.IsKeyDown(83 /* S */)) {
                this.position.y += 1;
            }
        };
        return Player;
    })(Box);
    exports.Player = Player;
    var Boid = (function (_super) {
        __extends(Boid, _super);
        function Boid() {
            _super.apply(this, arguments);
        }
        Boid.prototype.Update = function () {
        };
        return Boid;
    })(Box);
    exports.Boid = Boid;
});
//# sourceMappingURL=AppMain.js.map