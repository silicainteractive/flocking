/// <reference path="../modules/require.d.ts" />
/// <reference path="AppMain.ts" />
require.config({
    
});


require(['AppMain'], 
    (main) => {
        var app = new main.AppMain();
        app.Run();
}); 