import SilicaTS = require("lib/silica");
import State = SilicaTS.State;
import Game = SilicaTS.Game;
import Input = SilicaTS.Input;
import Keys = SilicaTS.Keys;
import Entity = SilicaTS.Entity;
import Vector2 = SilicaTS.Vector2;


export class AppMain {
    public Run() {
        var s = new State();
        var game = new Game(document.body, s);
        game.Start();

        s.AddChild(new Player(new Vector2(100, 100), new Vector2(20, 20)));
    }
}

export class Box extends Entity {

    constructor(position: Vector2, size: Vector2, public color: string = "black") {
        super(position, size);
    }

    public Draw(ctx: CanvasRenderingContext2D) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);
    }

}

export class Player extends Box {
    constructor(position: Vector2, size: Vector2, public color: string = "red") {
        super(position, size, color);
    }

    public Update() {
        if (Input.IsKeyDown(Keys.A)) {
            this.position.x -= 1;
        }
        if (Input.IsKeyDown(Keys.D)) {
            this.position.x += 1;
        }
        if (Input.IsKeyDown(Keys.W)) {
            this.position.y -= 1;
        }
        if (Input.IsKeyDown(Keys.S)) {
            this.position.y += 1;
        }
    }
}

export class Boid extends Box {
    public player: Player;

    public Update() {

    }

    
}
