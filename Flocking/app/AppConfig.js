/// <reference path="../modules/require.d.ts" />
/// <reference path="AppMain.ts" />
require.config({});
require(['AppMain'], function (main) {
    var app = new main.AppMain();
    app.Run();
});
//# sourceMappingURL=AppConfig.js.map